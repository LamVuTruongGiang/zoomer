var timeline = new TL.Timeline('timeline', 'content_event.json', {
    theme_color: "#288EC3",
    ga_property_id: "UA-27829802-4",
    slide_padding_lr: 50,
    timenav_height:250
});
let time_current=""
timeline.on('change', function(data) {
    var unique_id=data.unique_id;
    var current_main=data.target.config.events;
    var index=current_main.findIndex((curr)=>{
        return curr.unique_id==unique_id
    })
    if(index!=-1)
    {
        var current_data=current_main[index];
        time_current=current_data.start_date.data.year;
        $("#time_scoll").children().remove();
        $("#time_scoll").append('<span>'+time_current+'</span>');

    }
});
$(window).on("load",function () {
    $('.tl-timenav').before("<div id='head_nav' class='d-flex justify-content-center'>" +
        "<div id='time_scoll' class=''></div>" +
        "<div class='' id='timeline_search'><button id='btn_search'></button><input type='text' style='width: auto;background-color: transparent;border: none;margin-left: 5px;padding:0px;'></div></div>");

    var id_event_timeline = timeline.current_id;
    var current_content = timeline.config.events;
    var index=current_content.findIndex((curr)=>{
        return curr.current_id==id_event_timeline
    })
    if(index!=-1)
    {
        var current_data=current_main[index]
        time_current=current_data.start_date.data.year;
    }
    $("#time_scoll").append('<span>'+time_current+'</span>');
    $("#current_list").text(time_current);
});
