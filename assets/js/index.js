$(document).ready(function () {
    // $(".slick-slide").addClass("d-flex justify-content-center");
    $('#home__content__carousel img').css({ 'height': $(window).height() });
    $('.home__content .carousel-text').css({ 'margin-top': $(window).height() / 2 });
    if ($(window).width() <= 1024) {
        $('.overlay-content').removeClass('w-25');
    } else {
        $('.overlay-content').addClass('w-25');
    }
    //-------------------------------------------------------------------------------------------slick carousel
    $(".explore__slider").slick({
        infinite: true,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
    });
    $(".highlight_slider").slick({
        infinite: true,
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 1680,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $(".exhibits_slider").slick({
        infinite: true,
        // rows: 1,
        // slidesPerRow: 2,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        responsive: [
            {
                breakpoint: 1920,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            }
        ]
    });
    // $('.testimonials_slider').slick({
    //     slidesToShow: 2,
    //     // slidesPerRow: 2,
    //     rows:2,
    //     slidesToScroll: 1,
    //     infinite: true,
    //     dots: false,
    //     responsive: [
    //         {
    //
    //             breakpoint: 1768,
    //             settings: {
    //                 rows: 2,
    //                 slidesPerRow: 2,
    //                 // slidesToShow: 2,
    //                 slidesToScroll: 1,
    //                 infinite: true,
    //                 dots: false,
    //             }
    //         }, {
    //
    //             breakpoint: 992,
    //             settings: {
    //                 rows: 2,
    //                 slidesPerRow: 2,
    //                 slidesToShow: 1,
    //                 slidesToScroll: 1,
    //
    //             }
    //         }
    //     ]
    // });
    //-------------------------------------------------------------------------------------------slick carousel


    //-------------------------------------------------------------------------------------------click menu scroll content
    $("#myNav a[href*='#']:not([href='#])").click(function () {
        document.getElementById("myNav").style.height = "0%";
        let target = $(this).attr("href");
        $('html,body').stop().animate({
            scrollTop: $(target).offset().top
        }, 1000);
        event.preventDefault();
    });
    //-------------------------------------------------------------------------------------------click menu scroll content


    //-------------------------------------------------------------------------------------------click image home scroll top
    $("#image_home a[href*='#']:not([href='#])").click(function () {
        document.getElementById("image_home").style.height = "0%";
        let target = $(this).attr("href");
        $('html,body').stop().animate({
            scrollTop: $(target).offset().top
        }, 1000);

        event.preventDefault();
    });
    //-------------------------------------------------------------------------------------------click image home scroll top


    //-------------------------------------------------------------------------------------------click see more show more content
    $('#btn--appendEx').on('click', function () {
        $('.explore__content .explore__left__content').css("display", "block");
        $(this).css("display", "none");
        $('#btn--closedEx').css("display", "block");
    });
    $('#btn--closedEx').on('click', function () {
        $('.explore__content .explore__left__content').css("display", "-webkit-box");
        $(this).css("display", "none");
        $('#btn--appendEx').css("display", "block");
    });
    //$('#read_more').on('click', function () {
    //    $('.line_truncate').css("display", "block");
    //    $(this).css("display", "none");
    //    $('#back').css("display", "block");
    //});
    //$('#back').on('click', function () {
    //    $('.line_truncate').css("display", "-webkit-box");
    //    $(this).css("display", "none");
    //    $('#read_more').css("display", "block");
    //});
    //-------------------------------------------------------------------------------------------click see more show more content


    //-------------------------------------------------------------------------------------------hover highlight
    $('.slick-slide').addClass('d-flex justify-content-center');

    $(".highlight__slider__content").mouseenter(function () {
        $(this).parent().css({ "transition": "0.5s", "transform": " scale3d(1.10, 1.10, 1.10)" });
        $(this).css({ "transition": "0.5s", "transform": " scale3d(1.10, 1.10, 1.10)" });
    }).mouseleave(function () {
        $('.highlight__slider__img').css({ "transition": "0.5s", "transform": " scale3d(1, 1, 1)" });
        $(this).css({ "transition": "0.5s", "transform": " scale3d(1, 1, 1)" });
    });
    //-------------------------------------------------------------------------------------------hover highlight


    //-------------------------------------------------------------------------------------------click menu highlight show content
    $('#btn--appendEx').on('click', function () {
        $('.explore__content .explore--left__content').css("display", "block");
        $(this).css("display", "none");
        $('#btn--closedEx').css("display", "block");
    });
    $('#btn--closedEx').on('click', function () {
        $('.explore__content .explore--left__content').css("display", "-webkit-box");
        $(this).css("display", "none");
        $('#btn--appendEx').css("display", "block");
    });

    $('.inf__highligh').on('click', function () {
        $('#tabs__worldfair').addClass('active__tabs');
        $('#tabs__marilyn').removeClass('active__tabs');
    });
    $('.inf__highligh1').on('click', function () {
        $('#tabs__marilyn').addClass('active__tabs');
        $('#tabs__worldfair').removeClass('active__tabs');
    });


    $('.box--content1 button').on('click', function () {
        $('#tabs__marilyn__details').addClass('active__tabs');
        $('#tabs__worldfair').removeClass('active__tabs');
    });

    $('.box--left button').on('click', function () {
        $('#tabs__marilyn__details').addClass('active__tabs');
        $('#tabs__worldfair').removeClass('active__tabs');
    });

    $('.btnclosedet').on('click', function () {
        $('#tabs__worldfair').addClass('active__tabs');
        $('#tabs__marilyn__details').removeClass('active__tabs');
    });
    //-------------------------------------------------------------------------------------------click menu highlight show content


    //-------------------------------------------------------------------------------------------effect input testimonials
    $(".input-effect input").val("");
    $(".input-effect input").focusout(function () {
        if ($(this).val() != "") {
            $(this).addClass("has-content");
        } else {
            $(this).removeClass("has-content");
        }
    });
    //-------------------------------------------------------------------------------------------effect input testimonials
});

$(window).resize(function () {
    $('#home__content__carousel img').css({ 'height': $(window).height() });
    if ($(window).width() <= 1024) {
        $('.overlay-content').removeClass('w-25');
    } else {
        $('.overlay-content').addClass('w-25');
    }
});

//-------------------------------------------------------------------------------------------hamburger menu
function openNav() {
    document.getElementById("myNav").style.height = "100%";
    $('.menu__content__navbar').css("display", "none");
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
    $('.menu__content__navbar').css("display", "flex");
}

//-------------------------------------------------------------------------------------------hamburger menu

